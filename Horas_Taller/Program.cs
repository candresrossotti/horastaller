using Microsoft.EntityFrameworkCore;
using Horas_Taller.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<Hora_TallerContext>(option =>
{
    option.UseSqlServer(builder.Configuration.GetConnectionString("TrabajadorContext"));
});

builder.Services.AddDbContext<Hora_TallerContext>(option =>
{
    option.UseSqlServer(builder.Configuration.GetConnectionString("Internal_HorasContext"));
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
