﻿using Horas_Taller.Models;
using Horas_Taller.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Horas_Taller.Controllers
{
    public class InternalHorasController : Controller
    {
        private readonly Hora_TallerContext _context;//injecto la dependencia de la clase
                                                     //y le asigno como nombre context
                                                     //readonly es para solo lectura

        public InternalHorasController(Hora_TallerContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()//uso el metodo async por una cuestion de 
                                                //que es mejor usarlo para carga de datos de
                                                //un servidor
        {
            var emp = _context.Trabajadores.Include(b => b.Empleado);
            return View(await _context.InternalHoras.ToListAsync());

        }

        List<SelectListItem> tip_hora = new List<SelectListItem>();
        List<SelectListItem> tip_retraso = new List<SelectListItem>();
        List<SelectListItem> tall = new List<SelectListItem>();
        public IActionResult Create()
        {


            //De la siguiente manera llenamos manualmente,
            //Siendo el campo Text lo que ve el usuario y
            //el campo Value lo que en realidad vale nuestro valor
            tip_hora.Add(new SelectListItem() { Text = "Hora taller", Value = "Hora Taller" });
            tip_hora.Add(new SelectListItem() { Text = "Capacitacion", Value = "Capacitacion" });
            tip_hora.Add(new SelectListItem() { Text = "Hora supervisor", Value = "Hora supervisor" });
            tip_hora.Add(new SelectListItem() { Text = "Hora retraso", Value = "Hora retraso" });
            tip_hora.Add(new SelectListItem() { Text = "Luces Reglamentarias", Value = "Luces Reglamentarias" });
            tip_hora.Add(new SelectListItem() { Text = "Pruebas funcionales", Value = "Pruebas funcionales" });
            tip_hora.Add(new SelectListItem() { Text = "Trabajo en otro sector", Value = "Trabajo en otro sector" });
            ViewData["hor"] = new SelectList(tip_hora, "Value", "Text");

            ViewData["Nombree"] = new SelectList(_context.Trabajadores,"" ,"Empleado");
            //creamos una lista tipo SelectListItem

            //De la siguiente manera llenamos manualmente,
            //Siendo el campo Text lo que ve el usuario y
            //el campo Value lo que en realidad vale nuestro valor
            tip_retraso.Add(new SelectListItem() { Text = " ", Value = "1" });
            tip_retraso.Add(new SelectListItem() { Text = "Cambio de Diseño", Value = "Cambio de Diseño" });
            tip_retraso.Add(new SelectListItem() { Text = "Arreglo, corte y plegado", Value = "Arreglo, corte y plegado" });
            tip_retraso.Add(new SelectListItem() { Text = "Arreglo metalurgico", Value = "Arreglo metalurgico" });
            tip_retraso.Add(new SelectListItem() { Text = "Retoques pintura", Value = "Retoques pintura" });
            tip_retraso.Add(new SelectListItem() { Text = "Busqueda de Materiales", Value = "Busqueda de Materiales" });
            tip_retraso.Add(new SelectListItem() { Text = "Movimientos", Value = "Movimientos" });
            tip_retraso.Add(new SelectListItem() { Text = "Falta de herramientas", Value = "Falta de herramientas" });

            //ViewData va a ser lo que tenga la informacion
            ViewData ["retra"] = new SelectList(tip_retraso, "Value", "Text");

            tall.Add(new SelectListItem() { Text = "Montaje Electrico", Value = "Montaje Electrico" });
            ViewData["mont"] = new SelectList(tall, "Value", "Text");
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(InternalHorasViewModel model)
        {
            //De la siguiente manera llenamos manualmente,
            //Siendo el campo Text lo que ve el usuario y
            //el campo Value lo que en realidad vale nuestro valor
            tip_hora.Add(new SelectListItem() { Text = "Hora taller", Value = "Hora Taller" });
            tip_hora.Add(new SelectListItem() { Text = "Capacitacion", Value = "Capacitacion" });
            tip_hora.Add(new SelectListItem() { Text = "Hora supervisor", Value = "Hora supervisor" });
            tip_hora.Add(new SelectListItem() { Text = "Hora retraso", Value = "Hora retraso" });
            tip_hora.Add(new SelectListItem() { Text = "Luces Reglamentarias", Value = "Luces Reglamentarias" });
            tip_hora.Add(new SelectListItem() { Text = "Pruebas funcionales", Value = "Pruebas funcionales" });
            tip_hora.Add(new SelectListItem() { Text = "Trabajo en otro sector", Value = "Trabajo en otro sector" });
            ViewData["hor"] = new SelectList(tip_hora, "Value", "Text");

            ViewData["Nombree"] = new SelectList(_context.Trabajadores, "", "Empleado");
            //creamos una lista tipo SelectListItem

            //De la siguiente manera llenamos manualmente,
            //Siendo el campo Text lo que ve el usuario y
            //el campo Value lo que en realidad vale nuestro valor
            tip_retraso.Add(new SelectListItem() { Text = " ", Value = "1" });
            tip_retraso.Add(new SelectListItem() { Text = "Cambio de Diseño", Value = "Cambio de Diseño" });
            tip_retraso.Add(new SelectListItem() { Text = "Arreglo, corte y plegado", Value = "Arreglo, corte y plegado" });
            tip_retraso.Add(new SelectListItem() { Text = "Arreglo metalurgico", Value = "Arreglo metalurgico" });
            tip_retraso.Add(new SelectListItem() { Text = "Retoques pintura", Value = "Retoques pintura" });
            tip_retraso.Add(new SelectListItem() { Text = "Busqueda de Materiales", Value = "Busqueda de Materiales" });
            tip_retraso.Add(new SelectListItem() { Text = "Movimientos", Value = "Movimientos" });
            tip_retraso.Add(new SelectListItem() { Text = "Falta de herramientas", Value = "Falta de herramientas" });

            //ViewData va a ser lo que tenga la informacion
            ViewData["retra"] = new SelectList(tip_retraso, "Value", "Text");

            tall.Add(new SelectListItem() { Text = "Montaje Electrico", Value = "Montaje Electrico" });
            ViewData["mont"] = new SelectList(tall, "Value", "Text");
            if (ModelState.IsValid)
            {
                //creo variable IntHoras que va a ser la que se guarde en la base de datos
                var IntHoras = new InternalHora()
                {
                    Empleado = model.Empleado,
                    Proyecto = model.Proyecto,
                    Fecha = model.Fecha,
                    CantidadDeHoras = model.CantidadDeHoras,
                    Sector = model.Sector,
                    TipoRetraso = model.Tipo_Retraso,
                    TipoDeHora = model.TipoDeHora
                };
                //La agrego al contexto
                _context.Add(IntHoras);
                //Guardo el contexto en la base de datos
                await _context.SaveChangesAsync();

            }
            return View();
        }

        public async Task<IActionResult> Admin()
        {
            return View();
        }
        public async Task<IActionResult> Admin(InternalHorasViewModel model)
        {
            return View();
        }

    }
}
