﻿using Horas_Taller.Models;
using Horas_Taller.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;


namespace Horas_Taller.Controllers
{
    public class AdministradorController : Controller
    {
        private readonly Hora_TallerContext _context;//injecto la dependencia de la clase
                                                     //y le asigno como nombre context
                                                     //readonly es para solo lectura

        public AdministradorController(Hora_TallerContext context)
        {
            _context = context;
        }


        public IActionResult Index()
        {
            return View();
        }
    }
}
