﻿using Horas_Taller.Models;
using Horas_Taller.Models.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace Horas_Taller.Controllers
{
    public class LogInController : Controller
    {
        private readonly Hora_TallerContext _context;
        public LogInController(Hora_TallerContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()//uso el metodo async por una cuestion de 
                                                //que es mejor usarlo para carga de datos de
                                                //un servidor
        {

            return View(await _context.Trabajadores.ToListAsync());
        }


        public async Task<IActionResult> LogIn(TrabajadorViewModel model)
        {
            bool t = false;
            if (ModelState.IsValid)

            {
                var horas = new Trabajadore()
                {
                    Usuario = model.Usuario,
                    Contraseña = model.Contraseña,

                };
                foreach (var b in _context.Trabajadores)
                {
                    if (b.Contraseña==model.Contraseña && b.Usuario==model.Usuario)
                    {
                        t=true;
                    }
                }                
                /*_context.Add(horas);//guarda en el contexto pero no esta en la base de datos
                await _context.SaveChangesAsync();//guarda en la base de datos
                return RedirectToAction(nameof(Index));*/
            }
            if (t == false)
                return RedirectToAction(nameof(Index));
            else
                return RedirectToAction(nameof(LogIn));


        }
    }
}
