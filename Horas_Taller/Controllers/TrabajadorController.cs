﻿using Horas_Taller.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Horas_Taller.Controllers
{
    public class TrabajadorController : Controller
    {
        private readonly Hora_TallerContext _context;

        public TrabajadorController(Hora_TallerContext context)
        {
            _context = context;
        }
        public async Task<IActionResult> Index()//uso el metodo async por una cuestion de 
                                                //que es mejor usarlo para carga de datos de
                                                //un servidor
        {

            return View(await _context.Trabajadores.ToListAsync());
        }
    }
}