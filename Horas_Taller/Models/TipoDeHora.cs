﻿using System;
using System.Collections.Generic;

namespace Horas_Taller.Models
{
    public partial class TipoDeHora
    {
        public string HoraDeTaller { get; set; } = null!;
        public int IdHoraDeTaller { get; set; }
    }
}
