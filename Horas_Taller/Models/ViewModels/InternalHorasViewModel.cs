﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace Horas_Taller.Models.ViewModels
{
    //CLASE QUE SE USA PARA LLENAR UN FORMULARIO
    public class InternalHorasViewModel
    {
        [Required]
        [Display(Name ="Proyecto")]
        public string Proyecto { get; set; }

        [Required]
        [Display(Name = "Empleado")]
        public string Empleado { get; set; }
        [Required]
        [Display(Name = "Fecha")]
        public DateTime Fecha { get; set; }
        [Required]
        [Display(Name = "Sector")]
        public string? Sector { get; set; }
        [Required]
        [Display(Name = "Tipo de Hora")]
        public string? TipoDeHora { get; set; }
        [Required]
        [Display(Name = "Cantidad de Horas")]
        public int CantidadDeHoras { get; set; }
        [Display(Name = "Tipo de Retraso (opcional)")]
        public string Tipo_Retraso { get; set; }
    }

   
}
