﻿using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace Horas_Taller.Models.ViewModels
{
    public class TrabajadorViewModel
    {
        [Required]
        [Display(Name = "Empleado")]
        public string Empleado { get; set; }

      
        [Required]
        [Display(Name = "Usuario")]
        public string Usuario { get; set; }

        [Required]
        [Display(Name = "Contraseña")]
        public string Contraseña { get; set; }
    }
}