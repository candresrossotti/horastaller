﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Horas_Taller.Models
{
    public partial class Hora_TallerContext : DbContext
    {
        public Hora_TallerContext()
        {
        }

        public Hora_TallerContext(DbContextOptions<Hora_TallerContext> options)
            : base(options)
        {
        }

        public virtual DbSet<InternalHora> InternalHoras { get; set; } = null!;
        public virtual DbSet<TipoDeHora> TipoDeHoras { get; set; } = null!;
        public virtual DbSet<TipoDeRetraso> TipoDeRetrasos { get; set; } = null!;
        public virtual DbSet<Trabajadore> Trabajadores { get; set; } = null!;

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server= DESKTOP-4GBENUG\\TEW_SQLEXPRESS ; Database = Hora_Taller; Trusted_Connection=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<InternalHora>(entity =>
            {
                entity.HasKey(e => e.Id);

                entity.ToTable("Internal_Horas");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CantidadDeHoras).HasColumnName("Cantidad de Horas");

                entity.Property(e => e.Empleado)
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.Fecha).HasColumnType("date");

                entity.Property(e => e.HorasFichadas).HasColumnName("Horas Fichadas");

                entity.Property(e => e.Proyecto)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.Sector)
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.TipoDeHora)
                    .HasMaxLength(15)
                    .HasColumnName("Tipo de Hora")
                    .IsFixedLength();

                entity.Property(e => e.TipoRetraso)
                    .HasMaxLength(15)
                    .HasColumnName("Tipo Retraso")
                    .IsFixedLength();
            });

            modelBuilder.Entity<TipoDeHora>(entity =>
            {
                entity.HasKey(e => e.IdHoraDeTaller);

                entity.ToTable("Tipo_de_Hora");

                entity.Property(e => e.IdHoraDeTaller).HasColumnName("ID Hora de Taller");

                entity.Property(e => e.HoraDeTaller)
                    .HasMaxLength(40)
                    .HasColumnName("Hora de Taller")
                    .IsFixedLength();
            });

            modelBuilder.Entity<TipoDeRetraso>(entity =>
            {
                entity.HasKey(e => e.IdTipoDeRetraso);

                entity.ToTable("Tipo_De_Retraso");

                entity.Property(e => e.TipoDeRetraso1)
                    .HasMaxLength(30)
                    .HasColumnName("Tipo de Retraso")
                    .IsFixedLength();
            });

            modelBuilder.Entity<Trabajadore>(entity =>
            {
                entity.HasKey(e => e.Idempleado);

                entity.HasComment("tabla con datos de los chicos del taller");

                entity.Property(e => e.Idempleado).HasColumnName("IDEmpleado");

                entity.Property(e => e.Contraseña)
                    .HasMaxLength(30)
                    .IsFixedLength();

                entity.Property(e => e.Empleado)
                    .HasMaxLength(40)
                    .IsFixedLength();

                entity.Property(e => e.Sector)
                    .HasMaxLength(30)
                    .IsFixedLength();

                entity.Property(e => e.Usuario)
                    .HasMaxLength(30)
                    .IsFixedLength();
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
