﻿using System;
using System.Collections.Generic;

namespace Horas_Taller.Models
{
    public partial class InternalHora
    {
        public string? Empleado { get; set; }
        public DateTime? Fecha { get; set; }
        public int? HorasFichadas { get; set; }
        public string? Proyecto { get; set; }
        public string? TipoDeHora { get; set; }
        public int? CantidadDeHoras { get; set; }
        public string? Sector { get; set; }
        public int Id { get; set; }
        public string? TipoRetraso { get; set; }
    }
}
