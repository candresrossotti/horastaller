﻿using System;
using System.Collections.Generic;

namespace Horas_Taller.Models
{
    /// <summary>
    /// tabla con datos de los chicos del taller
    /// </summary>
    public partial class Trabajadore
    {
        public int Idempleado { get; set; }
        public string Empleado { get; set; } = null!;
        public string Sector { get; set; } = null!;
        public string? Usuario { get; set; }
        public string? Contraseña { get; set; }
    }
}
